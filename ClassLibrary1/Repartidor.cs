﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    class Repartidor: Persona
    {
        public List<int> CodigosPostalesHabilitados { get; set; }
        public double CargaMaxima { get; set; }

        public double CapacidadActual { get; set; }
    }
}
