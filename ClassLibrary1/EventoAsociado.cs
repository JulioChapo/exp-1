﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class EventoAsociado
    {
        public enum Asociado
        {
            LLEGADA_AL_CENTRO_DE_DISTRIBUCION,
            EN_VIAJE,
            EN_MANOS_DEL_REPARTIDOR,
            ENTREGADO
        }

        public Asociado TipoEvento { get; set; }

        public DateTime FechaEvento { get; set; }
    }
}
