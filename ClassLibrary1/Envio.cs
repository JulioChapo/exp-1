﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    class Envio
    {

        public int NroEnvio { get; set; }
        public int DniDestinatario { get; set; }
        public int DniRepartidor { get; set; }
        public DateTime FechaEntregaEstimada { get; set; }
        public DateTime FechaEntrega { get; set; }
        public double PesoEnvio { get; set; }
        public bool EnvioProgramado { get; set; }
        public string Descripcion { get; set; }
        public enum estados
        {
            PENDIENTE_ENVIO,
            ENVIADO,
            ULTIMO_TRAMO_RECORRIDO,
            ENTREGADO
        }
        public estados Estado { get; set; }
        public List<EventoAsociado> EventosAsociados { get; set; }

        public Envio()
        {
            Principal principal = new Principal();
            NroEnvio = principal.AutoIncrementarNroEnvio();
        }


        public string Actualizar(EventoAsociado nEvento) {

            EventoAsociado nuevoEvento = new EventoAsociado();
            EventoAsociado ultimoEvento = EventosAsociados.Last();
            bool exito = false;

            if ((nEvento.TipoEvento == EventoAsociado.Asociado.EN_VIAJE) && (ultimoEvento.TipoEvento == EventoAsociado.Asociado.LLEGADA_AL_CENTRO_DE_DISTRIBUCION))
            {
                nuevoEvento.TipoEvento = nEvento.TipoEvento;
                nuevoEvento.FechaEvento = DateTime.Now;
                Estado = estados.ENVIADO;
                EventosAsociados.Add(nuevoEvento);
                exito = true;
                return $"{exito}";
            }

            if ((nEvento.TipoEvento == EventoAsociado.Asociado.EN_MANOS_DEL_REPARTIDOR) && (ultimoEvento.TipoEvento == EventoAsociado.Asociado.EN_VIAJE))
            {
                nuevoEvento.TipoEvento = nEvento.TipoEvento;
                nuevoEvento.FechaEvento = DateTime.Now;
                Estado = estados.ULTIMO_TRAMO_RECORRIDO;
                EventosAsociados.Add(nuevoEvento);
                exito = true;
                return $"{exito}";
            }

            if ((nEvento.TipoEvento == EventoAsociado.Asociado.ENTREGADO) && (ultimoEvento.TipoEvento == EventoAsociado.Asociado.EN_MANOS_DEL_REPARTIDOR))
            {
                nuevoEvento.TipoEvento = nEvento.TipoEvento;
                nuevoEvento.FechaEvento = DateTime.Now;
                Estado = estados.ENTREGADO;
                EventosAsociados.Add(nuevoEvento);
                exito = true;
                return $"{exito}";
            }
            return $"Se produjo un error, {exito}";
        }
    }
}
