﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    class Principal
    {
        List<Envio> Envios = new List<Envio>();
        List<EventoAsociado> EventosAsociados = new List<EventoAsociado>();
        List<Repartidor> Repartidores = new List<Repartidor>();


        public int AutoIncrementarNroEnvio()
        {
            return Envios.Count() + 1;
        }


        public int CrearNuevoEnvio (int dniDestinatario, DateTime fechaEstimada, string descripcionPaquete, double peso)
        {
            Envio nuevoEnvio = new Envio();
            EventoAsociado nuevoEvento = new EventoAsociado();
            nuevoEnvio.DniDestinatario = dniDestinatario;
            nuevoEnvio.FechaEntregaEstimada = fechaEstimada;
            nuevoEnvio.Descripcion = descripcionPaquete;
            nuevoEnvio.EnvioProgramado = false;
            nuevoEnvio.PesoEnvio = peso;
            nuevoEvento.FechaEvento = DateTime.Now;
            nuevoEvento.TipoEvento = EventoAsociado.Asociado.LLEGADA_AL_CENTRO_DE_DISTRIBUCION;
            nuevoEnvio.Estado = Envio.estados.PENDIENTE_ENVIO;
            nuevoEnvio.EventosAsociados.Add(nuevoEvento);
            Envios.Add(nuevoEnvio);
           
         
            return nuevoEnvio.NroEnvio;
        }


        public string ActualizarEnvio(int nroEnvio, EventoAsociado evento)
        {
            Envio envioEncontrado = Envios.Find(x => x.NroEnvio == nroEnvio);
            return envioEncontrado.Actualizar(evento);
            
        }


        public void AsignarEnvio()
        {
            List<Envio> enviosSinAsignar = Envios.FindAll(x => x.EnvioProgramado == false);
            enviosSinAsignar.OrderByDescending(x => x.PesoEnvio );
            List<Destinatario> Destinatarios = new List<Destinatario>();

            foreach (var envio in enviosSinAsignar)
            {
                Destinatario destinatario = Destinatarios.Find(x => x.DNI == envio.DniDestinatario);
                foreach (var repartidor in Repartidores)
                {
                    foreach (var codigoPostal in repartidor.CodigosPostalesHabilitados)
                    {
                        if (repartidor.CodigosPostalesHabilitados[codigoPostal] == destinatario.CodigoPostal)
                        {
                            if (envio.PesoEnvio + repartidor.CapacidadActual <= repartidor.CargaMaxima)
                            {
                                envio.EnvioProgramado = true;
                                envio.DniRepartidor = repartidor.DNI;
                                repartidor.CapacidadActual += envio.PesoEnvio;

                                Envios.Remove(Envios.Find(x=>x.NroEnvio == envio.NroEnvio));                               
                                Envios.Add(envio);
                                Envios.OrderBy(x => x.NroEnvio);
                            }
                        }
                    }
                }
            }
        }
    }
}
